Stavba abstraktny syntaxticky strom pre rozne programcie jazyky<br>
Cielom prace bude zahrnat stavbu nejakeho jednotneho AST pre rozne jazyky, ako je napr. Java, Python, PHP a Javascript, ktory bude neskor pouzity v bakalarskej praci na vyhodnotenie empirickej statistiky a softverove metriky zdrojovych suborov.<br>
Veduci projektu: RNDr. Richard Ostertag, PhD.